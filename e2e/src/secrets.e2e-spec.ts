import { browser, by, ExpectedConditions as EC } from "protractor";
import { ProtractorBrowser } from "protractor/built/browser";
import { isExtension } from "./app.po";
import { GroupsPage } from "./group.po";
import { SecretsPage } from "./secrets.po";
import { DEFAULT_WAIT, login, register } from "./shared";

(isExtension ? xdescribe : describe)("List, Add Secrets", () => {
  let groupsPage: GroupsPage;
  let secretsPage: SecretsPage;
  let browser2: ProtractorBrowser;

  let USERNAME: string;
  let PASSWORD: string;
  let USERNAME2: string;
  let PASSWORD2: string;

  let groupName: string;
  let secretName: string;

  beforeAll(async () => {
    browser2 = browser.forkNewDriverInstance();
    browser2
      .manage()
      .timeouts()
      .setScriptTimeout(30 * 1000);
    secretsPage = new SecretsPage();
    groupsPage = new GroupsPage();

    browser.sleep(50);

    USERNAME =
      Math.random()
        .toString(36)
        .slice(2) + "@example.com";
    PASSWORD = "hunterhunter22";
    USERNAME2 =
      Math.random()
        .toString(36)
        .slice(2) + "@example.com";
    PASSWORD2 = "hunterhunter22";
    groupName = "test";
    secretName = "Test Secret";

    await register(browser, USERNAME, PASSWORD, PASSWORD);
    await browser.refresh();
    await login(browser, USERNAME, PASSWORD);
  });

  afterAll(async () => {
    browser2.close();
    browser.executeScript("window.localStorage.clear();");
    browser.executeScript("window.sessionStorage.clear();");
  });

  it("in groups page and logged in", async () => {
    await groupsPage.navigateTo();
    const currentUrl = await browser.getCurrentUrl();
    expect(currentUrl.endsWith("groups")).toBeTruthy();
  });

  it("It creates a group and adds second user to group", async () => {
    // Create second user
    await register(browser2, USERNAME2, PASSWORD2, PASSWORD2);
    await browser2.refresh();
    await login(browser2, USERNAME2, PASSWORD2);

    // First user creates group
    browser.refresh(); // Cheap way to refresh contacts data
    await groupsPage.newGroup(browser);
    await groupsPage.enterGroupName(groupName);

    const elem = groupsPage.getMembersInput();
    browser
      .actions()
      .mouseMove(elem)
      .click();
    // First user adds second user to group
    await groupsPage.getMembersInput().sendKeys(USERNAME2);
    const groupClick = groupsPage.getDropDownItem(USERNAME2);
    await groupClick.click();
    await EC.browser.wait(
      EC.presenceOf(groupsPage.groupMember(USERNAME2)),
      DEFAULT_WAIT,
      "Group member is added"
    );

    await groupsPage.submitGroupByEnter();
    await browser.wait(
      EC.stalenessOf(groupsPage.getFormElem()),
      DEFAULT_WAIT,
      "group not saved and form is still open"
    );
  });

  it("Creates a secret and adds a group", async () => {
    await secretsPage.navigateTo(browser);
    const searchElem = secretsPage.getSearchElem();
    await EC.browser.wait(
      EC.presenceOf(searchElem),
      DEFAULT_WAIT,
      "Can see list"
    );
    await searchElem.isDisplayed();

    await secretsPage.selectAddNewPassword();
    await secretsPage.enterName(secretName);
    await secretsPage.enterUsername("mctest");
    await secretsPage.enterPassword("hunter2");

    await secretsPage.groupsSelect().click();
    const group = groupsPage.getDropDownItem(groupName);
    await group.click();

    await secretsPage.submitByEnter();
  });

  it("Decrypts the shared password from a group", async () => {
    browser2.refresh();
    const currentSecretToggleBtnElem = secretsPage.getToggleButton(
      browser2,
      secretName
    );
    const button = currentSecretToggleBtnElem
      .element(secretsPage.getManageSecretToggleBtnElem(browser2))
      .locator();
    button.click();

    const formElement = secretsPage.getFormElem(browser2);
    await browser.wait(
      EC.presenceOf(formElement),
      DEFAULT_WAIT,
      "Secret Form not present"
    );

    const currentPasswordToggleBtnElem = browser2.element(
      by.className("t-password-viewer__actions")
    );
    currentPasswordToggleBtnElem.click();

    const passwordNameText = async () => {
      const passwordNameElem = await secretsPage.getPasswordNameElem(browser2);
      return passwordNameElem;
    };

    expect(await passwordNameText()).toBe("hunter2");

    const closeButton = secretsPage.getManageSecretToggleBtnElem(browser2);
    await closeButton.click();
  });
});
