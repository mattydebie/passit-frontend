import { storiesOf, moduleMetadata } from "@storybook/angular";
import { withKnobs, boolean, text, select } from "@storybook/addon-knobs";
import { RouterTestingModule } from "@angular/router/testing";
import { TextFieldComponent } from "~/app/shared/text-field/text-field.component";
import { AsideLinkComponent } from "~/app/shared/aside-link/aside-link.component";
import { FormLabelComponent } from "~/app/shared/form-label/form-label.component";
import { NgrxFormsModule } from "ngrx-forms";
import { InlineSVGModule } from "ng-inline-svg";
import { StoreModule } from "@ngrx/store";
import { HttpClientModule } from "@angular/common/http";

storiesOf("Shared", module)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [
        RouterTestingModule,
        NgrxFormsModule,
        HttpClientModule,
        InlineSVGModule.forRoot(),
        StoreModule.forRoot({})
      ],
      declarations: [AsideLinkComponent, FormLabelComponent]
    })
  )
  .add("Aside Link", () => ({
    template: `
      <aside-link>
        Here's an aside <em>link</em>!
      </aside-link>
    `
  }))
  .add("Text Input", () => ({
    component: TextFieldComponent,
    props: {
      ngrxFormControl: { id: "test", value: "", errors: {} },
      label: text("label", "My Field"),
      isComplete: boolean("isComplete", false),
      type: select(
        "type",
        { "": null, email: "email", password: "password", url: "url" } as any,
        undefined as any
      )
    }
  }));
