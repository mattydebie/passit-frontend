import { take } from "rxjs/operators";
import { fakeAsync, inject, TestBed } from "@angular/core/testing";
import { Store, StoreModule, select } from "@ngrx/store";

import { logout } from "../app.reducers";
import {
  LoginAction,
  LoginSuccessAction,
  LogoutSuccessAction
} from "./account.actions";
import * as fromAccount from "./account.reducer";
import * as fromLogin from "./login/login.reducer";
import { IAuthState } from "./account.reducer";
import { IAuthStore } from "./user/";

describe("AccountReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;

      const result = fromAccount.authReducer(undefined, action);
      expect(result).toEqual(fromAccount.initialState);
    });
  });

  describe("wrong login payload", () => {
    it("should NOT authenticate a user", () => {
      const createAction = new LoginAction();

      const startState = {
        ...fromAccount.initialState,
        login: { ...fromLogin.initialState, form: { email: "test" } }
      };
      const expectedResult = fromAccount.initialState;

      const result = fromAccount.authReducer(startState, createAction);
      expect(result).toEqual(expectedResult);
    });
  });

  describe("LOGIN_SUCCESS", () => {
    it("should add a user set loggedIn to true in auth state", () => {
      const user: IAuthStore = {
        email: "test@example.com",
        privateKey: "fake",
        publicKey: "fake",
        userId: 1,
        userToken: "aaa",
        rememberMe: false,
        optInErrorReporting: false
      };
      const createAction = new LoginSuccessAction(user);

      const expectedResult: IAuthState = {
        email: "test@example.com",
        userId: 1,
        privateKey: "fake",
        publicKey: "fake",
        userToken: "aaa",
        url: fromAccount.initialState.url,
        rememberMe: false,
        optInErrorReporting: false,
        forceSetPassword: false
      };

      const result = fromAccount.authReducer(
        fromAccount.initialState,
        createAction
      );
      expect(result).toEqual(expectedResult);
      // Mimic slice of global state
      const accountState = {
        account: {
          auth: result
        }
      };
      // selector should say we are now logged in
      expect(fromAccount.getIsLoggedIn(accountState)).toBe(true);
    });
  });

  describe("LOGOUT", () => {
    beforeEach(() => {
      // Bootstrap the logout metareducer
      const metaReducers = [logout];
      TestBed.configureTestingModule({
        imports: [
          StoreModule.forRoot({}, { metaReducers }),
          StoreModule.forFeature("account", fromAccount.reducers)
        ],
        providers: []
      });
    });

    it("should logout a user", fakeAsync(
      inject([Store], (store: Store<any>) => {
        const isLoggedInSelector = store.pipe(
          select(fromAccount.getIsLoggedIn)
        );

        // Login first
        const user: IAuthStore = {
          email: "test@example.com",
          privateKey: "fake",
          publicKey: "fake",
          userId: 1,
          userToken: "aaa",
          rememberMe: false,
          optInErrorReporting: false
        };
        store.dispatch(new LoginSuccessAction(user));
        isLoggedInSelector
          .pipe(take(1))
          .subscribe(isLoggedIn => expect(isLoggedIn).toBe(true));

        // Now logout
        store.dispatch(new LogoutSuccessAction());
        isLoggedInSelector
          .pipe(take(1))
          .subscribe(isLoggedIn => expect(isLoggedIn).toBe(false));
      })
    ));
  });
});
