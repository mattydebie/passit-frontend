import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { NgrxFormsModule } from "ngrx-forms";

import { reducers } from "./account.reducer";
import { SharedModule } from "../shared/shared.module";
import { DirectivesModule } from "../directives";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { LoginEffects } from "./account.effects";
import { LoginFormEffects } from "./login/login.effects";
import { RegisterEffects } from "./register/register.effects";
import { ConfirmEmailEffects } from "./confirm-email/confirm-email.effects";
import { DownloadBackupCodeComponent } from "./manage-backup-code/download-backup-code/download-backup-code.component";
import { ServerSelectComponent } from "./shared/server-select.component";
import { ErrorReportingEffects } from "./error-reporting/error-reporting.effects";
import { DeleteComponent } from "./delete/delete.component";
import { ResetPasswordEffects } from "./reset-password/reset-password.effects";
import { ResetPasswordVerifyEffects } from "./reset-password/reset-password-verify/reset-password-verify.effects";
import { SetPasswordEffects } from "./reset-password/set-password/set-password.effects";
import { ManageBackupCodeEffects } from "./manage-backup-code/manage-backup-code.effects";
import { RegisterContainer } from "./register/register.container";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { LoginContainer } from "./login/login.container";
import { UserService } from "./user";
import { ConfirmEmailGuard } from "./confirm-email/confirm-email.guard";
import { BackupCodeComponent } from "./backup-code/backup-code.component";
import { BackupCodePdfService } from "./backup-code-pdf.service";
import { ConfirmEmailComponent, ConfirmEmailContainer } from "./confirm-email";
import { ErrorReportingComponent } from "./error-reporting/error-reporting.component";
import { ErrorReportingContainer } from "./error-reporting/error-reporting.container";
import {
  ChangePasswordComponent,
  ChangePasswordContainer
} from "./change-password";
import { TNSLoginEffects } from "./login/tns-login-effects";
import { MobileMenuModule } from "../mobile-menu";
import { ResetPasswordContainer } from "./reset-password/reset-password.container";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { ForgotLearnMoreContainer } from "./change-password/forgot-learn-more/forgot-learn-more.container";
import { ForgotLearnMoreComponent } from "./change-password/forgot-learn-more/forgot-learn-more.component";
import { ResetPasswordVerifyComponent } from "./reset-password/reset-password-verify/reset-password-verify.component";
import { SetPasswordComponent } from "./reset-password/set-password/set-password.component";
import { ManageBackupCodeComponent } from "./manage-backup-code/manage-backup-code.component";
import { PasswordInputComponent } from "./change-password/password-input/password-input.component";
import { MarketingFrameComponent } from "./marketing-frame/marketing-frame.component";

export const COMPONENTS = [
  LoginContainer,
  LoginComponent,
  RegisterComponent,
  RegisterContainer,
  ConfirmEmailComponent,
  ConfirmEmailContainer,
  BackupCodeComponent,
  ChangePasswordComponent,
  ChangePasswordContainer,
  ResetPasswordComponent,
  ResetPasswordContainer,
  ErrorReportingComponent,
  ErrorReportingContainer,
  ForgotLearnMoreContainer,
  ForgotLearnMoreComponent,
  DownloadBackupCodeComponent,
  DeleteComponent,
  ServerSelectComponent,
  ResetPasswordVerifyComponent,
  SetPasswordComponent,
  ManageBackupCodeComponent,
  PasswordInputComponent,
  MarketingFrameComponent
];

export const SERVICES = [BackupCodePdfService, UserService, ConfirmEmailGuard];

@NgModule({
  imports: [
    DirectivesModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NgrxFormsModule,
    SharedModule,
    ProgressIndicatorModule,
    MobileMenuModule,
    StoreModule.forFeature("account", reducers),
    EffectsModule.forFeature([
      LoginEffects,
      TNSLoginEffects,
      LoginFormEffects,
      RegisterEffects,
      ConfirmEmailEffects,
      ErrorReportingEffects,
      ResetPasswordEffects,
      ResetPasswordVerifyEffects,
      SetPasswordEffects,
      ManageBackupCodeEffects
    ])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SERVICES],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AccountModule {}
