import {
  createFeatureSelector,
  createSelector,
  ActionReducerMap
} from "@ngrx/store";
import { AccountActions, AccountActionTypes } from "./account.actions";
import * as fromConfirmEmail from "./confirm-email/confirm-email.reducer";
import * as fromLogin from "./login/login.reducer";
import {
  RegisterActionTypes,
  RegisterAction,
  RegisterSuccessAction
} from "./register/register.actions";
import {
  ErrorReportingTypes,
  ErrorReportingActionsUnion
} from "./error-reporting/error-reporting.actions";
import * as fromRegister from "./register/register.reducer";
import * as fromChangePassword from "./change-password/change-password.reducer";
import * as fromDeleteAccount from "./delete/delete.reducer";
import * as fromErrorReporting from "./error-reporting/error-reporting.reducer";
import * as fromResetPassword from "./reset-password/reset-password.reducer";
import * as fromResetPasswordVerify from "./reset-password/reset-password-verify/reset-password-verify.reducer";
import * as fromSetPassword from "./reset-password/set-password/set-password.reducer";
import {
  ResetPasswordVerifyActionTypes,
  ResetPasswordVerifyActionsUnion
} from "./reset-password/reset-password-verify/reset-password-verify.actions";
import {
  SetPasswordSuccess,
  SetPasswordActionTypes,
  ForceSetPassword
} from "./reset-password/set-password/set-password.actions";
import * as fromManageBackupCode from "./manage-backup-code/manage-backup-code.reducer";
import {
  ChangePasswordSubmitFormSuccess,
  ChangePasswordActionTypes
} from "./change-password/change-password.actions";

export interface IAuthState {
  email: string | null;
  userId: number | null;
  url: string | null;
  userToken: string | null;
  privateKey: string | null;
  publicKey: string | null;
  rememberMe: boolean;
  optInErrorReporting: boolean;
  /** Always redirect logged in user to set password page */
  forceSetPassword: boolean;
}

export const initialState: IAuthState = {
  email: null,
  userId: null,
  url: null,
  userToken: null,
  privateKey: null,
  publicKey: null,
  rememberMe: false,
  optInErrorReporting: false,
  forceSetPassword: false
};

export function authReducer(
  state = initialState,
  action:
    | AccountActions
    | RegisterAction
    | RegisterSuccessAction
    | ErrorReportingActionsUnion
    | ResetPasswordVerifyActionsUnion
    | SetPasswordSuccess
    | ForceSetPassword
    | ChangePasswordSubmitFormSuccess
): IAuthState {
  switch (action.type) {
    case AccountActionTypes.LOGIN:
    case RegisterActionTypes.REGISTER:
      // Clear any stale state (except the url)
      return { ...initialState, url: state.url };

    case AccountActionTypes.LOGIN_SUCCESS:
    case RegisterActionTypes.REGISTER_SUCCESS:
    case ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN_SUCCESS:
      return {
        ...state,
        email: action.payload.email,
        userId: action.payload.userId,
        userToken: action.payload.userToken,
        privateKey: action.payload.privateKey,
        publicKey: action.payload.publicKey,
        rememberMe: action.payload.rememberMe,
        optInErrorReporting: action.payload.optInErrorReporting
      };

    case ChangePasswordActionTypes.SUBMIT_FORM_SUCCESS:
      return {
        ...state,
        userToken: action.payload.token,
        privateKey: action.payload.privateKey,
        publicKey: action.payload.publicKey
      };

    case AccountActionTypes.SET_URL:
      return {
        ...state,
        url: action.payload
      };

    case SetPasswordActionTypes.SET_PASSWORD_SUCCESS:
      return {
        ...state,
        forceSetPassword: false
      };

    case ErrorReportingTypes.SAVE_FORM_SUCCESS:
      return {
        ...state,
        optInErrorReporting: action.payload.opt_in_error_reporting
      };

    case SetPasswordActionTypes.FORCE_SET_PASSWORD:
      return {
        ...state,
        forceSetPassword: true
      };

    default:
      return state;
  }
}

export interface IAccountState {
  auth: IAuthState;
  login: fromLogin.ILoginState;
  register: fromRegister.IRegisterState;
  confirmEmail: fromConfirmEmail.IConfirmEmailState;
  changePassword: fromChangePassword.IChangePasswordState;
  deleteAccount: fromDeleteAccount.IDeleteAccountState;
  errorReporting: fromErrorReporting.IState;
  resetPassword: fromResetPassword.IResetPasswordState;
  resetPasswordVerify: fromResetPasswordVerify.IResetPasswordVerifyState;
  manageBackupCode: fromManageBackupCode.IState;
  setPassword: fromSetPassword.ISetPasswordState;
}

export const reducers: ActionReducerMap<IAccountState> = {
  auth: authReducer,
  login: fromLogin.reducer,
  register: fromRegister.reducer,
  confirmEmail: fromConfirmEmail.reducer,
  changePassword: fromChangePassword.reducer,
  deleteAccount: fromDeleteAccount.reducer,
  errorReporting: fromErrorReporting.reducer,
  resetPassword: fromResetPassword.reducer,
  resetPasswordVerify: fromResetPasswordVerify.reducer,
  setPassword: fromSetPassword.reducer,
  manageBackupCode: fromManageBackupCode.reducer
};

export const selectAccountState = createFeatureSelector<IAccountState>(
  "account"
);
export const selectLoginState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.login
);
export const getLoginHasStarted = createSelector(
  selectLoginState,
  fromLogin.getHasStarted
);
export const getLoginHasFinished = createSelector(
  selectLoginState,
  fromLogin.getHasFinished
);
export const getLoginErrorMessage = createSelector(
  selectLoginState,
  fromLogin.getErrorMessage
);
export const getLoginForm = createSelector(
  selectLoginState,
  fromLogin.getForm
);

export const selectAuthState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.auth
);
export const getIsLoggedIn = createSelector(
  selectAuthState,
  (state: IAuthState) => (state.userToken ? true : false)
);
export const getToken = createSelector(
  selectAuthState,
  (state: IAuthState) => state.userToken
);
export const getEmail = createSelector(
  selectAuthState,
  (state: IAuthState) => state.email
);
export const getForceUserSetPassword = createSelector(
  selectAuthState,
  (state: IAuthState) => state.forceSetPassword && state.userToken // Sanity check user is logged in
);
export const getUserId = createSelector(
  selectAuthState,
  (state: IAuthState) => state.userId
);
export const getUrl = createSelector(
  selectAuthState,
  (state: IAuthState) => state.url
);
export const getOptInErrorReporting = createSelector(
  selectAuthState,
  (state: IAuthState) => state.optInErrorReporting
);

export const getLoginState = (state: IAccountState) => state.login;

export const selectRegisterState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.register
);

export const getRegisterForm = createSelector(
  selectRegisterState,
  fromRegister.getForm
);

export const getUrlForm = createSelector(
  selectRegisterState,
  fromRegister.getUrlForm
);

export const getHasSubmitStarted = createSelector(
  selectRegisterState,
  fromRegister.getHasSubmitStarted
);

export const getHasSubmitFinished = createSelector(
  selectRegisterState,
  fromRegister.getHasSubmitFinished
);

export const getRegisterErrorMessage = createSelector(
  selectRegisterState,
  fromRegister.getErrorMessage
);

export const getRegisterBackupCode = createSelector(
  selectRegisterState,
  fromRegister.getBackupCode
);

export const getStage = createSelector(
  selectRegisterState,
  fromRegister.getStage
);

export const getUrlDisplayName = createSelector(
  selectRegisterState,
  fromRegister.getUrlDisplayName
);

export const getShowUrl = createSelector(
  selectRegisterState,
  fromRegister.getShowUrl
);

export const getRegisterIsEmailTaken = createSelector(
  selectRegisterState,
  fromRegister.getIsEmailTaken
);

export const getisUrlValid = createSelector(
  selectRegisterState,
  fromRegister.getIsUrlValid
);

export const selectConfirmEmailState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.confirmEmail
);

export const getConfirmEmailMessage = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getResetCodeMessage
);
export const getConfirmIsVerified = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getIsVerified
);
export const getConfirmHasStarted = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getHasStarted
);
export const getConfirmHasFinished = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getHasFinished
);
export const getConfirmErrorMessage = createSelector(
  selectConfirmEmailState,
  fromConfirmEmail.getErrorMessage
);

export const selectChangePasswordForm = createSelector(
  selectAccountState,
  (state: IAccountState) => state.changePassword
);
export const changePassword = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getForm
);
export const changePasswordHasStarted = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getHasStarted
);
export const changePasswordHasFinished = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getHasFinished
);
export const selectChangePasswordNewBackupCode = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getNewBackupCode
);
export const changePasswordErrorMessage = createSelector(
  selectChangePasswordForm,
  fromChangePassword.getErrorMessage
);

export const selectDeleteAccountState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.deleteAccount
);
export const getDeleteErrorMessage = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getErrorMessage
);
export const getDeleteForm = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getForm
);
export const getDeleteHasStarted = createSelector(
  selectDeleteAccountState,
  fromDeleteAccount.getHasStarted
);

export const selectErrorReportingState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.errorReporting
);
export const getErrorReportingForm = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getForm
);
export const getErrorReportingHasFinished = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getHasFinished
);
export const getErrorReportingHasStarted = createSelector(
  selectErrorReportingState,
  fromErrorReporting.getHasStarted
);

export const selectResetPasswordState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.resetPassword
);
export const getResetPasswordForm = createSelector(
  selectResetPasswordState,
  fromResetPassword.getForm
);
export const getResetPasswordHasStarted = createSelector(
  selectResetPasswordState,
  fromResetPassword.getHasStarted
);
export const getResetPasswordHasFinished = createSelector(
  selectResetPasswordState,
  fromResetPassword.getHasFinished
);
export const getResetPasswordErrorMessage = createSelector(
  selectResetPasswordState,
  fromResetPassword.getErrorMessage
);

export const selectManageBackupCodeForm = createSelector(
  selectAccountState,
  (state: IAccountState) => state.manageBackupCode
);

export const manageBackupCode = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getForm
);

export const manageBackupCodeHasStarted = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getHasStarted
);

export const manageBackupCodeHasFinished = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getHasFinished
);

export const selectManageBackupCodeNewBackupCode = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getNewBackupCode
);

export const manageBackupCodeErrorMessage = createSelector(
  selectManageBackupCodeForm,
  fromManageBackupCode.getErrorMessage
);

export const selectResetPasswordVerifyState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.resetPasswordVerify
);
export const getResetPasswordVerifyForm = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getForm
);
export const getResetPasswordVerifyHasStarted = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getHasStarted
);
export const getResetPasswordVerifyErrorMessage = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getErrorMessage
);
export const getResetPasswordVerifyEmailAndCode = createSelector(
  selectResetPasswordVerifyState,
  fromResetPasswordVerify.getEmailAndCode
);

export const selectSetPasswordState = createSelector(
  selectAccountState,
  (state: IAccountState) => state.setPassword
);
export const getSetPasswordForm = createSelector(
  selectSetPasswordState,
  fromSetPassword.getForm
);
export const getSetPasswordHasStarted = createSelector(
  selectSetPasswordState,
  fromSetPassword.getHasStarted
);
export const getSetPasswordBackupCode = createSelector(
  selectSetPasswordState,
  fromSetPassword.getBackupCode
);
