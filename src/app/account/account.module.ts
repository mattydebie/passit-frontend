import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { InlineSVGModule } from "ng-inline-svg";
import { NgrxFormsModule } from "ngrx-forms";

import { AccountComponent } from "./account.component";
import { LoginEffects } from "./account.effects";
import { reducers } from "./account.reducer";
import {
  ChangePasswordComponent,
  ChangePasswordContainer
} from "./change-password";
import { ChangePasswordEffects } from "./change-password/change-password.effects";
import { ConfirmEmailComponent, ConfirmEmailContainer } from "./confirm-email";
import { ConfirmEmailEffects } from "./confirm-email/confirm-email.effects";
import { DeleteComponent } from "./delete/delete.component";
import { DeleteContainer } from "./delete/delete.container";
import { RegisterEffects } from "./register/register.effects";

import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { SharedModule } from "../shared/shared.module";
import { LoginFormEffects } from "./login/login.effects";
import { ErrorReportingComponent } from "./error-reporting/error-reporting.component";
import { ErrorReportingContainer } from "./error-reporting/error-reporting.container";
import { ErrorReportingEffects } from "./error-reporting/error-reporting.effects";
import { BackupCodeComponent } from "./backup-code/backup-code.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { MarketingFrameComponent } from "./marketing-frame/marketing-frame.component";
import { ResetPasswordContainer } from "./reset-password/reset-password.container";
import { ResetPasswordEffects } from "./reset-password/reset-password.effects";
import { ResetPasswordVerifyComponent } from "./reset-password/reset-password-verify/reset-password-verify.component";
import { ResetPasswordVerifyContainer } from "./reset-password/reset-password-verify/reset-password-verify.container";
import { ResetPasswordVerifyEffects } from "./reset-password/reset-password-verify/reset-password-verify.effects";
import { PasswordInputComponent } from "./change-password/password-input/password-input.component";
import { ManageBackupCodeComponent } from "./manage-backup-code/manage-backup-code.component";
import { ManageBackupCodeContainer } from "./manage-backup-code/manage-backup-code.container";
import { ManageBackupCodeEffects } from "./manage-backup-code/manage-backup-code.effects";
import { DownloadBackupCodeComponent } from "./manage-backup-code/download-backup-code/download-backup-code.component";
import { BackupCodePdfService } from "./backup-code-pdf.service";
import { SetPasswordComponent } from "./reset-password/set-password/set-password.component";
import { SetPasswordContainer } from "./reset-password/set-password/set-password.container";
import { SetPasswordEffects } from "./reset-password/set-password/set-password.effects";
import { ServerSelectComponent } from "./shared/server-select.component";
import { RegisterContainer } from "./register/register.container";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { LoginContainer } from "./login/login.container";
import { ForgotLearnMoreContainer } from "./change-password/forgot-learn-more/forgot-learn-more.container";
import { ForgotLearnMoreComponent } from "./change-password/forgot-learn-more/forgot-learn-more.component";
import { ConfirmEmailGuard } from "./confirm-email/confirm-email.guard";
import { UserService } from "./user";
import { DeleteEffects } from "./delete/delete.effects";

export const COMPONENTS = [
  LoginContainer,
  LoginComponent,
  RegisterComponent,
  RegisterContainer,
  AccountComponent,
  ChangePasswordComponent,
  ChangePasswordContainer,
  PasswordInputComponent,
  ConfirmEmailComponent,
  ConfirmEmailContainer,
  DeleteContainer,
  DeleteComponent,
  ErrorReportingComponent,
  ErrorReportingContainer,
  BackupCodeComponent,
  DownloadBackupCodeComponent,
  ResetPasswordComponent,
  ResetPasswordContainer,
  ResetPasswordVerifyComponent,
  ResetPasswordVerifyContainer,
  SetPasswordComponent,
  MarketingFrameComponent,
  SetPasswordContainer,
  ManageBackupCodeComponent,
  ManageBackupCodeContainer,
  MarketingFrameComponent,
  ServerSelectComponent,
  ForgotLearnMoreContainer,
  ForgotLearnMoreComponent
];

export const SERVICES = [BackupCodePdfService, UserService, ConfirmEmailGuard];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InlineSVGModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    NgrxFormsModule,
    ProgressIndicatorModule,
    StoreModule.forFeature("account", reducers),
    EffectsModule.forFeature([
      LoginEffects,
      LoginFormEffects,
      RegisterEffects,
      ConfirmEmailEffects,
      ErrorReportingEffects,
      ResetPasswordEffects,
      ResetPasswordVerifyEffects,
      SetPasswordEffects,
      ManageBackupCodeEffects,
      ChangePasswordEffects,
      DeleteEffects
    ])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SERVICES]
})
export class AccountModule {}
