import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  FormGroupState,
  validate,
  ValidationErrors,
  updateGroup
} from "ngrx-forms";
import { oldPasswordValidators, passwordValidators } from "../constants";
import { equalTo, required } from "ngrx-forms/validation";
import {
  ChangePasswordActionTypes,
  ChangePasswordActionsUnion
} from "./change-password.actions";
import { IBaseFormState } from "~/app/utils/interfaces";

export const FORM_ID = "Change Password Form";

export interface IChangePasswordForm {
  oldPassword: string;
  newPassword: string;
  newPasswordConfirm: string;
  showConfirm: boolean;
}

export interface IChangePasswordState extends IBaseFormState {
  form: FormGroupState<IChangePasswordForm>;
  errorMessage: string[] | null;
  newBackupCode: string | null;
}

export function notEqualTo<T>(comparand: T) {
  return (value: T): ValidationErrors => {
    if (value !== comparand) {
      return {};
    }

    return {
      notEqualTo: {
        comparand,
        actual: value
      }
    };
  };
}

export const validateAndUpdateFormState = updateGroup<IChangePasswordForm>({
  oldPassword: validate(oldPasswordValidators),
  newPassword: (newPassword, form) => {
    return validate(newPassword, [
      ...passwordValidators,
      notEqualTo(form.value.oldPassword)
    ]);
  },
  newPasswordConfirm: (passwordConfirm, form) => {
    if (form.controls.showConfirm.value) {
      return validate(
        passwordConfirm,
        required,
        equalTo(form.value.newPassword)
      );
    }
    return validate(passwordConfirm, []);
  }
});

export const newBackupCode = (state: IChangePasswordState) =>
  state.newBackupCode;

const initialFormState = validateAndUpdateFormState(
  createFormGroupState<IChangePasswordForm>(FORM_ID, {
    oldPassword: "",
    newPassword: "",
    newPasswordConfirm: "",
    showConfirm: true
  })
);

export const initialState = {
  form: initialFormState,
  hasFinished: false,
  hasStarted: false,
  newBackupCode: null,
  errorMessage: null
};

const formReducer = createFormStateReducerWithUpdate<IChangePasswordForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: ChangePasswordActionsUnion
): IChangePasswordState {
  const form = formReducer(state.form, action);
  state = { ...state, form };

  switch (action.type) {
    case ChangePasswordActionTypes.SUBMIT_FORM:
      return {
        ...state,
        hasStarted: true,
        hasFinished: false,
        errorMessage: null,
        newBackupCode: null
      };

    case ChangePasswordActionTypes.SUBMIT_FORM_SUCCESS:
      return {
        ...state,
        hasFinished: true,
        newBackupCode: action.payload.backupCode,
        errorMessage: null,
        hasStarted: false
      };

    case ChangePasswordActionTypes.SUBMIT_FORM_FAILURE:
      return {
        ...state,
        errorMessage: action.payload,
        hasFinished: false,
        hasStarted: false,
        newBackupCode: null
      };

    case ChangePasswordActionTypes.RESET_FORM:
      return initialState;
  }

  return state;
}

export const getForm = (state: IChangePasswordState) => state.form;
export const getErrorMessage = (state: IChangePasswordState) =>
  state.errorMessage;
export const getHasStarted = (state: IChangePasswordState) => state.hasStarted;
export const getHasFinished = (state: IChangePasswordState) =>
  state.hasFinished;
export const getNewBackupCode = (state: IChangePasswordState) =>
  state.newBackupCode;
