import {
  createFormGroupState,
  createFormStateReducerWithUpdate,
  FormGroupState,
  validate,
  updateGroup
} from "ngrx-forms";
import { passwordValidators } from "../../constants";
import { equalTo, required } from "ngrx-forms/validation";
import {
  SetPasswordActionsUnion,
  SetPasswordActionTypes
} from "./set-password.actions";
import {
  VerifyAndLogin,
  ResetPasswordVerifyActionTypes
} from "../reset-password-verify/reset-password-verify.actions";

export const FORM_ID = "(Reset) Set Password Form";

export interface ISetPasswordForm {
  newPassword: string;
  newPasswordConfirm: string;
  showConfirm: boolean;
}

export interface ISetPasswordState {
  form: FormGroupState<ISetPasswordForm>;
  hasStarted: boolean;
  backupCode: string | null;
}

export const validateAndUpdateFormState = updateGroup<ISetPasswordForm>({
  newPassword: (newPassword, form) => {
    return validate(newPassword, [...passwordValidators]);
  },
  newPasswordConfirm: (passwordConfirm, form) => {
    if (form.controls.showConfirm.value) {
      return validate(
        passwordConfirm,
        required,
        equalTo(form.value.newPassword)
      );
    }
    return validate(passwordConfirm, []);
  }
});

const initialFormState = validateAndUpdateFormState(
  createFormGroupState<ISetPasswordForm>(FORM_ID, {
    newPassword: "",
    newPasswordConfirm: "",
    showConfirm: true
  })
);

export const initialState: ISetPasswordState = {
  form: initialFormState,
  hasStarted: false,
  backupCode: null
};

const formReducer = createFormStateReducerWithUpdate<ISetPasswordForm>(
  validateAndUpdateFormState
);

export function reducer(
  state = initialState,
  action: SetPasswordActionsUnion | VerifyAndLogin
): ISetPasswordState {
  const form = formReducer(state.form, action);
  if (form !== state.form) {
    state = { ...state, form };
  }

  switch (action.type) {
    case SetPasswordActionTypes.SET_PASSWORD:
      return {
        ...state,
        hasStarted: true
      };

    case SetPasswordActionTypes.SET_PASSWORD_FAILURE:
      return {
        ...state,
        hasStarted: false
      };

    case SetPasswordActionTypes.SET_PASSWORD_SUCCESS:
      return {
        ...state,
        hasStarted: false,
        backupCode: null
      };

    case ResetPasswordVerifyActionTypes.VERIFY_AND_LOGIN:
      return {
        ...state,
        backupCode: action.payload
      };
  }

  return state;
}

export const getForm = (state: ISetPasswordState) => state.form;
export const getHasStarted = (state: ISetPasswordState) => state.hasStarted;
export const getBackupCode = (state: ISetPasswordState) => state.backupCode;
