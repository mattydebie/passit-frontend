import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { Store, select } from "@ngrx/store";
import { IState } from "../../../app.reducers";
import { UserService } from "../../user";
import {
  SetPassword,
  SetPasswordActionTypes,
  SetPasswordSuccess,
  SetPasswordFailure
} from "./set-password.actions";
import { mergeMap, withLatestFrom, map, catchError } from "rxjs/operators";
import {
  getSetPasswordForm,
  getResetPasswordVerifyEmailAndCode,
  getSetPasswordBackupCode
} from "~/app/account/account.reducer";
import { of } from "rxjs";

@Injectable()
export class SetPasswordEffects {
  @Effect()
  setPassword$ = this.actions$.pipe(
    ofType<SetPassword>(SetPasswordActionTypes.SET_PASSWORD),
    withLatestFrom(this.store.pipe(select(getSetPasswordForm))),
    withLatestFrom(this.store.pipe(select(getResetPasswordVerifyEmailAndCode))),
    withLatestFrom(this.store.pipe(select(getSetPasswordBackupCode))),
    mergeMap(([[[action, form], emailCode], backupCode]) =>
      this.userService
        .resetPasswordSetPassword(
          emailCode.emailCode!,
          form.value.newPassword,
          backupCode!
        )
        .pipe(
          map(() => new SetPasswordSuccess()),
          catchError(() => of(new SetPasswordFailure()))
        )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private userService: UserService
  ) {}
}
