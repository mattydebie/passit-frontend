import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StoreModule } from "@ngrx/store";
import { TooltipModule } from "ngx-tooltip";
import { FormsModule } from "@angular/forms";

import { ImporterContainer } from "./importer.container";
import { ImporterComponent } from "./importer.component";
import { ImporterService } from "./importer.service";
import { LoggedInGuard } from "../guards";
import { importerReducer } from "./importer.reducer";
import { InlineSVGModule } from "ng-inline-svg";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";

const routes: Routes = [
  {
    path: "import",
    component: ImporterContainer,
    canActivate: [LoggedInGuard],
    data: {
      title: "Import Passwords"
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    InlineSVGModule,
    ProgressIndicatorModule,
    FormsModule,
    TooltipModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature("importer", importerReducer)
  ],
  exports: [RouterModule],
  declarations: [ImporterContainer, ImporterComponent],
  providers: [ImporterService]
})
export class ImporterModule {}
