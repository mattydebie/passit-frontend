import { ExporterComponent } from "./exporter.component";
import { LoggedInGuard } from "../guards";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ExporterService } from "./exporter.service";
import { FormsModule } from "@angular/forms";

const routes: Routes = [
  {
    path: "export",
    canActivate: [LoggedInGuard],
    component: ExporterComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule],
  exports: [RouterModule],
  declarations: [ExporterComponent],
  providers: [ExporterService]
})
export class ExporterModule {}
