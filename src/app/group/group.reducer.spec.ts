import { IContact } from "~/app/group/contacts/contacts.interfaces";
import { IGroupForm } from "../group/group.interfaces";
import { ISelectOptions } from "./group-form/group-form.interfaces";
import * as GroupActions from "./group.actions";
import { IGroup } from "./group.interfaces";
import * as fromGroup from "./group.reducer";
import { IGroupState } from "./group.reducer";
import { ResetForms } from "../form/reset-form.actions";

describe("GroupReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;
      const result = fromGroup.groupReducer(undefined, action);
      expect(result).toEqual(fromGroup.initialState);
    });
  });

  describe("ADD_GROUP", () => {
    it("should add a group", () => {
      const addedGroup: IGroup = {
        id: 1,
        name: "text",
        slug: "text",
        public_key: "text",
        groupuser_set: [],
        my_key_ciphertext: "text",
        my_private_key_ciphertext: "text"
      };

      const createAction = new GroupActions.AddGroup(addedGroup);
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.entities[addedGroup.id]).toEqual(addedGroup);
    });
  });

  describe("REMOVE_GROUP", () => {
    it("should remove a group", () => {
      const removedGroup: IGroup = {
        id: 1,
        name: "text",
        slug: "text",
        public_key: "text",
        groupuser_set: [],
        my_key_ciphertext: "text",
        my_private_key_ciphertext: "text"
      };

      const createAction = new GroupActions.RemoveGroup(removedGroup.id);
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result).not.toContain(removedGroup);
    });
  });

  describe("getPendingGroups", () => {
    it("should only get groups where the user is pending acceptance", () => {
      const groupState: fromGroup.IGroupState = {
        ...fromGroup.initialState,
        ids: [1, 2],
        entities: {
          1: {
            id: 1,
            name: "Pending Group",
            slug: "",
            public_key: "",
            groupuser_set: [
              {
                id: 1,
                user: 2,
                group: 1,
                is_group_admin: true,
                is_invite_pending: false
              },
              {
                id: 2,
                user: 1,
                group: 1,
                is_group_admin: true,
                is_invite_pending: true
              }
            ],
            my_key_ciphertext: "",
            my_private_key_ciphertext: ""
          },
          2: {
            id: 2,
            name: "Is my group, but already accepted",
            slug: "",
            public_key: "",
            groupuser_set: [
              {
                id: 2,
                user: 1,
                group: 2,
                is_group_admin: true,
                is_invite_pending: false
              }
            ],
            my_key_ciphertext: "",
            my_private_key_ciphertext: ""
          }
        }
      };
      const result = fromGroup.getPendingGroups.projector(
        fromGroup.getGroups.projector(groupState),
        1
      );
      expect(result.length).toBe(1);
    });
  });

  describe("RESET_BLANK_GROUP_FORM", () => {
    it("should reset a blank group form", () => {
      const createAction = new GroupActions.ResetBlankGroupForm();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result).toEqual(fromGroup.initialState);
    });
  });

  describe("RESET_FORMS", () => {
    it("It should reset ephemeral data but not groups and contacts", () => {
      const state: fromGroup.IGroupState = {
        ...fromGroup.initialState,
        groupIsUpdating: !fromGroup.initialState.groupIsUpdating,
        ids: [1],
        entities: {
          1: {
            id: 1,
            name: "text",
            slug: "text",
            public_key: "text",
            groupuser_set: [],
            my_key_ciphertext: "text",
            my_private_key_ciphertext: "text"
          }
        }
      };
      const action = new ResetForms();
      const result = fromGroup.groupReducer(state, action);
      expect(result.groupIsUpdating).toEqual(
        fromGroup.initialState.groupIsUpdating
      );
      expect(result.entities).toEqual(state.entities);
    });
  });

  describe("SET_GROUP", () => {
    it("should set groups", () => {
      const setGroup: IGroup[] = [
        {
          id: 1,
          name: "text",
          slug: "text",
          public_key: "text",
          groupuser_set: [],
          my_key_ciphertext: "text",
          my_private_key_ciphertext: "text"
        }
      ];

      const createAction = new GroupActions.SetGroupsAction(setGroup);
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.entities[1]).toEqual(setGroup[0]);
    });
  });

  describe("SHOW_GROUPS_CREATE", () => {
    it("should show groups create", () => {
      const createAction = new GroupActions.ShowGroupsCreate();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.showCreate).toEqual(true);
    });
  });

  describe("CREATE_GROUP", () => {
    it("should change groupIsUpdating to true", () => {
      const createAction = new GroupActions.CreateGroupAction();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.groupIsUpdating).toEqual(true);
    });
  });

  describe("CREATE_GROUP_SUCCESS", () => {
    it("should change groupIsUpdating to false and groupIsUpdated to true", () => {
      const createAction = new GroupActions.CreateGroupSuccessAction();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.groupIsUpdating).toEqual(false);
      expect(result.groupIsUpdated).toEqual(true);
    });
  });

  describe("UPDATE_GROUP_SUCCESS should show the group has finished saving.", () => {
    it("should show is updating is no longer true", () => {
      const createAction = new GroupActions.UpdateGroupSuccessAction();
      const startState: fromGroup.IGroupState = {
        ...fromGroup.initialState,
        groupManaged: 1
      };
      const result = fromGroup.groupReducer(startState, createAction);

      expect(result.groupIsUpdating).toEqual(false);
      expect(result.groupIsUpdated).toEqual(true);
      // This should remain as we don't want to close a group on save.
      expect(result.groupManaged).toEqual(1);
    });
  });

  describe("HIDE_GROUPS_CREATE", () => {
    it("should hide a group", () => {
      const createAction = new GroupActions.HideGroupsCreate();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result).toEqual(fromGroup.initialState);
    });
  });

  describe("CLEAR_MANAGED_GROUP", () => {
    it("should clear a managed group", () => {
      const createAction = new GroupActions.ClearManagedGroupAction();
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.groupManaged).toEqual(null);
    });
  });

  describe("SET_MANAGED_GROUP", () => {
    it(`should display a group with id equal to payload and set groupForm name equal
        to found group name`, () => {
      const testState: IGroupState = fromGroup.initialState;

      const group: IGroup = {
        id: 1,
        name: "test",
        slug: "text",
        public_key: "text",
        groupuser_set: [],
        my_key_ciphertext: "text",
        my_private_key_ciphertext: "text"
      };

      const state: IGroupState = {
        ...testState,
        ids: [1],
        entities: { 1: group }
      };

      const createAction = new GroupActions.SetManagedGroupAction(1);
      const result = fromGroup.groupReducer(state, createAction);

      expect(result.groupManaged).toEqual(1);
      expect(result.groupForm.name).toEqual(group.name);
    });
  });

  describe("DELETE_GROUP", () => {
    it("should delete a group", () => {
      const deletedGroup: IGroup = {
        id: 1,
        name: "text",
        slug: "text",
        public_key: "text",
        groupuser_set: [],
        my_key_ciphertext: "text",
        my_private_key_ciphertext: "text"
      };

      const createAction = new GroupActions.DeleteGroupAction(deletedGroup.id);
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result).not.toContain(deletedGroup);
    });
  });

  describe("CONTACT_LOOKUP_SUCCESS", () => {
    it("should return a user id and email if user email exists and set contactLookup to user", () => {
      const searchedUser = {
        value: 1,
        label: "test@example.com",
        disabled: false
      };

      const createAction = new GroupActions.ContactLookupSuccessAction(
        searchedUser
      );
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.contactLookup).toBe(searchedUser);
    });
  });

  describe("UPDATE_FORM", () => {
    it("should update groupForm state when group form values change", () => {
      const groupForm: IGroupForm = {
        name: "test@example.com",
        members: []
      };

      const createAction = new GroupActions.UpdateFormAction(groupForm);
      const result = fromGroup.groupReducer(
        fromGroup.initialState,
        createAction
      );

      expect(result.groupForm).toEqual(groupForm);
    });
  });
});

describe("getGroupContacts", () => {
  it("should return no contacts when no contacts exist (sanity check)", () => {
    const testState: IGroupState = fromGroup.initialState;
    const fullState: any = {
      group: testState
    };
    const filteredContacts: IContact[] = fromGroup.getContacts(fullState);
    expect(filteredContacts).toEqual([]);
  });
});

describe("getGroupMembersForDisplay", () => {
  it("should return no contacts when no contacts exist (sanity check)", () => {
    const testState: IGroupState = fromGroup.initialState;
    const fullState: any = {
      group: { ...testState }
    };
    const filteredContacts:
      | ISelectOptions[]
      | undefined = fromGroup.getGroupMembersForDisplay(fullState);
    expect(filteredContacts).toEqual(undefined);
  });
});

describe("getGroupForm", () => {
  it("should not return group form information when group does not exist (sanity check)", () => {
    const testState: IGroupState = fromGroup.initialState;
    const fullState: any = {
      group: { ...testState }
    };
    const groupFormState: IGroupForm = fromGroup.getGroupForm(fullState);
    expect(groupFormState).toEqual({ members: [], name: "" });
  });

  it("should return group form information when group exists", () => {
    const testState: IGroupState = fromGroup.initialState;
    const groupForm = {
      members: [1],
      name: "test"
    };
    const fullState: any = {
      group: {
        ...testState,
        groupForm
      }
    };
    const groupFormState: IGroupForm = fromGroup.getGroupForm(fullState);
    expect(groupFormState).toEqual({ members: [1], name: "test" });
  });
});
