import { TestBed } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { StoreModule } from "@ngrx/store";
import { cold, hot } from "jasmine-marbles";
import { Observable } from "rxjs";

import {
  GetContactsAction,
  SetContactsAction
} from "./contacts/contacts.actions";
import { ContactsService } from "./contacts/contacts.service";
import {
  ContactLookupAction,
  ContactLookupSuccessAction
} from "./group.actions";
import { GroupEffects } from "./group.effects";
import { groupReducer } from "./group.reducer";
import { IGroupState } from "./group.reducer";
import { GroupService } from "./group.service";
import { testContact, testContacts } from "./test_data";

import { HandleAPIErrorAction } from "../account/account.actions";

const mockState: IGroupState = {
  groupForm: {
    members: [],
    name: ""
  },
  errorMessage: null,
  groupManaged: null,
  ids: [],
  entities: {},
  showCreate: false,
  groupIsUpdating: false,
  groupIsUpdated: false,
  contactLookup: null,
  newContacts: [],
  contacts: {
    ids: [],
    entities: {}
  }
};

describe("Group Effects", () => {
  let effects: GroupEffects;
  let contactsService: any;
  let actions: Observable<any>;
  const contacts = testContacts;
  const contact = testContact;
  const emailLookup = "test@gmail.com";

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({}),
        StoreModule.forFeature("group", groupReducer, {
          initialState: mockState
        }),
        RouterTestingModule
      ],
      providers: [
        GroupEffects,
        provideMockActions(() => actions),
        {
          provide: ContactsService,
          useValue: jasmine.createSpyObj("ContactsService", [
            "getContacts",
            "contactLookup"
          ])
        },
        {
          provide: GroupService,
          useValue: jasmine.createSpyObj("GroupService", [
            "getContacts",
            "contactLookup"
          ])
        }
      ]
    });
    effects = TestBed.get(GroupEffects);
    contactsService = TestBed.get(ContactsService);
  });

  it("should get and set contacts", () => {
    actions = hot("a", { a: new GetContactsAction() });
    const response = cold("-a", { a: contacts });
    contactsService.getContacts.and.returnValue(response);
    const expected = cold("-b", { b: new SetContactsAction(contacts) });
    expect(effects.getContacts$).toBeObservable(expected);
  });

  xit("should handle errors on get contacts", () => {
    actions = hot("a", { a: new GetContactsAction() });
    const response = cold("-#", {});
    contactsService.getContacts.and.returnValue(response);
    const expected = cold("-b#", { b: new HandleAPIErrorAction("error") });
    expect(effects.getContacts$).toBeObservable(expected);
  });

  xit("should lookup a contact by email and return an id if user exists", () => {
    actions = hot("a", { a: new ContactLookupAction(emailLookup) });
    const response = cold("-a", { a: contact });
    contactsService.contactLookup.and.returnValue(response);
    const expected = cold("-b", { b: new ContactLookupSuccessAction(contact) });
    expect(effects.contactLookup$).toBeObservable(expected);
  });
});
