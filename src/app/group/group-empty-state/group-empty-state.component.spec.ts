import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { GroupEmptyStateComponent } from "./group-empty-state.component";

describe("GroupEmptyStateComponent", () => {
  let component: GroupEmptyStateComponent;
  let fixture: ComponentFixture<GroupEmptyStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupEmptyStateComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupEmptyStateComponent);
    component = fixture.componentInstance;
    component.pendingInviteGroups = [];
    component.groups = [];
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
