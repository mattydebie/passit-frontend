import { HttpErrorResponse } from "@angular/common/http";

export interface IDRFResponse {
  _body: any;
  json: () => object;
  status: number;
  statusText: string;
}

export interface IPassitSDKError {
  res: HttpErrorResponse;
}
