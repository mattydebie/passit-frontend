import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { DeviceType } from "ui/enums";
import { device } from "platform";
import { Router } from "@angular/router";
import { tap, filter } from "rxjs/operators";
import {
  RemoveSecretAction,
  SecretActionTypes
} from "../../secrets/secret.actions";

export const IS_TABLET = device.deviceType === DeviceType.Tablet;

@Injectable()
export class TNSSecretFormEffects {
  @Effect({ dispatch: false })
  removeSecret$ = this.actions$.pipe(
    ofType<RemoveSecretAction>(SecretActionTypes.REMOVE_SECRET),
    filter(action => !IS_TABLET),
    tap(() => this.router.navigate(["/list"]))
  );

  constructor(private actions$: Actions, private router: Router) {}
}
