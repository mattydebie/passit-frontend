import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ActionBarContainer } from "./action-bar.container";
import { ActionBarComponent } from "./action-bar.component";
import { mobileMenuReducer } from "./mobile-menu.reducers";
import { SideDrawer } from "./sidedrawer";
import { MobileMenuEffects } from "./mobile-menu.effects";

const COMPONENTS = [ActionBarComponent, ActionBarContainer];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    StoreModule.forFeature("mobile-menu", mobileMenuReducer),
    EffectsModule.forFeature([MobileMenuEffects])
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [SideDrawer],
  schemas: [NO_ERRORS_SCHEMA]
})
export class MobileMenuModule {}
