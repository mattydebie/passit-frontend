import { Action } from "@ngrx/store";

export enum MobileMenuActionTypes {
  TOGGLE_MENU = "[Mobile Menu] Toggle Menu"
}

export class ToggleMenu implements Action {
  readonly type = MobileMenuActionTypes.TOGGLE_MENU;
}

export type MobileMenuActions = ToggleMenu;
