import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgrxFormsModule } from "ngrx-forms";

import { ButtonComponent } from "./button/button.component";
import { TextFieldComponent } from "./text-field/text-field.component";
import { HeadingComponent } from "./heading/heading.component";
import { SearchComponent } from "./search/search.component";
import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { DirectivesModule } from "../directives";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { AsideLinkComponent } from "./aside-link/aside-link.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";

export const COMPONENTS = [
  ButtonComponent,
  TextFieldComponent,
  HeadingComponent,
  CheckboxComponent,
  AsideLinkComponent,
  SearchComponent,
  NonFieldMessagesComponent
];

@NgModule({
  imports: [
    NativeScriptCommonModule,
    NgrxFormsModule,
    DirectivesModule,
    NativeScriptRouterModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  schemas: [NO_ERRORS_SCHEMA]
})
export class SharedModule {}
