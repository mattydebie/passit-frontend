import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ElementRef
} from "@angular/core";
import { AbstractControlState } from "ngrx-forms";

@Component({
  selector: "app-checkbox",
  template: `
    <FlexboxLayout flexDirection="row" alignItems="flex-start" justifyContent="space-between">
        <StackLayout>
          <Label
              class="text-label m-b-5 m-t-2"
              [text]="title"
              textWrap="true"
          ></Label>
          <Label
              class="text-label text-label--light m-r-5"
              [text]="subtext"
              textWrap="true"
          ></Label>
        </StackLayout>
      <Switch #switch
            [ngrxFormControlState]="control"
            class="app-switch"
        ></Switch>
    </FlexboxLayout>
  `,
  styles: [
    `
      .app-switch {
        width: 100;
        background-color: #6f989e;
        color: #6f989e;
      }
      .app-switch[checked="true"] {
        width: 100;
        background-color: #0092a8;
        color: #0092a8;
      }
    `
  ],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent {
  @Input()
  title: string;
  @Input()
  subtext: string;
  @Input()
  inline = false;
  @Input()
  control: AbstractControlState<boolean>;
  @ViewChild("switch")
  switch: ElementRef;

  focusInput = () => {
    this.switch.nativeElement.focus();
  };
}
