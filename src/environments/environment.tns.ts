export const environment = {
  production: false,
  extension: false,
  docker: false,
  nativescript: true,
  hmr: false,
  VERSION: require("../../package.json").version
};
